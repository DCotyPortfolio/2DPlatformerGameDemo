cmake_minimum_required(VERSION 3.10)
project(platformer)
set(CMAKE_CXX_STANDARD 11)
add_executable(platformer main.cpp Game.h Game.cpp SpriteComponent.h SpriteComponent.cpp Actor.h Actor.cpp Ship.h Ship.cpp Math.h Math.cpp Component.h Component.cpp BGSpriteComponent.h BGSpriteComponent.cpp AnimSpriteComponent.h AnimSpriteComponent.cpp Main.cpp)

#adding SDL library

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${PROJECT_SOURCE_DIR}/cmake")

find_package(SDL2 REQUIRED)
find_package(SDL2_image REQUIRED)
#find_package(SDL2_ttf REQUIRED)

include_directories(${SDL2_INCLUDE_DIR} ${SDL2_IMAGE_INCLUDE_DIR} ${SDL2_TTF_INCLUDE_DIR})
target_link_libraries(${CMAKE_PROJECT_NAME} ${SDL2_LIBRARY} ${SDL2_IMAGE_LIBRARIES} ${SDL2_TTF_LIBRARIES})
#target_link_libraries(${SDL2_LIBRARY} ${SDL2_IMAGE_LIBRARIES} ${SDL2_TTF_LIBRARIES})






