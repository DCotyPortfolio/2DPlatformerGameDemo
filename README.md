This is a sample 2D platform game i am currently working on in C++, mostly for personal development reasons. This is the example project in a great book on C++ game programming by Sanjay Madhav, I highly recommend you purchase the book at https://www.amazon.com/Game-Programming-Creating-Games-Design/dp/0134597206/ref=sr_1_1?ie=UTF8&qid=1529202419&sr=8-1&keywords=game+programming+in+C%2B%2B

**How this project is set up**

I am using the following to work through the book:

* My laptop( a macbook)
* JetBrains CLion - The original source was designed to run in XCode, but i modified it a bit to run in CLion, that's just my preference.
* SDL Libraries ( more on the setup below)
* The C++ book mentioned above

The project has Some assets that were part of the original source code for the book. I did not draw up the assets, i'm just using them to get the game going, again as i am working through the book. 


**SDL Libraries setup, using a Mac**

* get the SDL2, SDL2_image, and SDL2_ttf libraries from https://www.libsdl.org/
* download the .dmg files and put them in /Library/Frameworks Directory
* Use the files in the cmake folder in this project to point the project to the libraries.

**Special setup for CLion**
* If you play with the code in this project in another project you create, be sure to adjust add_executable
in the CMakeLists.txt file for every file you add to the project, or else you will get compile errors.

* BE SURE you Adjust your Build configurations to specify a working directory, 
or else the assets won't load properly. You do this under run -> Edit Configurations. You should probably do this every time you use CLion anyway, it seems that this doesn't always get set for you.

* Don't use Brew to download the SDL libraries for this project. I got various errors using the libs off brew.


**Support for this application does not exist!!**
Please feel free to use the code to brush up on your code kata and for learning, but as this code mostly came from a book i am working out of, Please do no not use this code or the assets for commercial purposes.


