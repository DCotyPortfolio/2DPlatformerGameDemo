//
// Created by David on 6/16/18.
//

#ifndef PLATFORMER_COMPONENT_H
#define PLATFORMER_COMPONENT_H

#endif //PLATFORMER_COMPONENT_H



#pragma once
class Component
{
public:
    // Constructor
    // (the lower the update order, the earlier the component updates)
    Component(class Actor* owner, int updateOrder = 100);
    // Destructor
    virtual ~Component();
    // Update this component by delta time
    virtual void Update(float deltaTime);

    int GetUpdateOrder() const { return mUpdateOrder; }
protected:
    // Owning actor
    class Actor* mOwner;
    // Update order of component
    int mUpdateOrder;
};

