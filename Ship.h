//
// Created by David on 6/16/18.
//

#ifndef PLATFORMER_SHIP_H
#define PLATFORMER_SHIP_H

#endif //PLATFORMER_SHIP_H


#pragma once
#include "Actor.h"
class Ship : public Actor
{
public:
    Ship(class Game* game);
    void UpdateActor(float deltaTime) override;
    void ProcessKeyboard(const uint8_t* state);
    float GetRightSpeed() const { return mRightSpeed; }
    float GetDownSpeed() const { return mDownSpeed; }
private:
    float mRightSpeed;
    float mDownSpeed;
};